# DataServer1.py

from threading import Thread
import socket
import time
import sys

DEBUG_MODE = True
IP_PORT = 22000

def debug(text):
    if DEBUG_MODE:
        print("Debug:---", text)

# ---------------------- class SocketHandler ------------------------
class SocketHandler(Thread):
    def __init__(self, conn):
        Thread.__init__(self)
        self.conn = conn

    def run(self):
        global isConnected
        debug("SocketHandler started")
        while True:
            cmd = [];
            cmdStr = "";
            try:
                debug("Calling blocking conn.recv()")
                cmd = self.conn.recv(1024)
            except:
                debug("exception in conn.recv()")
                # happens when connection is reset from the peer
                break
            debug(type(cmd))
            if type(cmd) == bytes:
                debug("Received bytes")
                cmdStr = cmd.decode()
            else:
                debug("Received type " + type(cmd))
                if type(cmd) == str:
                    cmdStr = cmd;
                else:
                    debug(".. Unable to adopt type " + type(cmd))
            debug("Received cmd: " + cmdStr + " len: " + str(len(cmdStr)))
            if len(cmdStr) == 0:
                break
        conn.close()
        print("Client disconnected. Waiting for next client...")
        isConnected = False
        debug("SocketHandler terminated")

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# close port when process exits:
serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
debug("Socket created")
HOSTNAME = "" # Symbolic name meaning all available interfaces
try:
    serverSocket.bind((HOSTNAME, IP_PORT))
except socket.error as msg:
    print("Bind failed", msg[0], msg[1])
    sys.exit()
serverSocket.listen(10)

print("Waiting for a connecting client...")
isConnected = False
while True:
    debug("Calling blocking accept()...")
    conn, addr = serverSocket.accept()
    print("Connected with client at " + addr[0])
    isConnected = True
    socketHandler = SocketHandler(conn)
    # necessary to terminate it at program termination:
    socketHandler.setDaemon(True)
    socketHandler.start()
    t = 0
    while isConnected:
        print("Server runtime ", t, "s")
        time.sleep(10)
        t += 10
