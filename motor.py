from time import sleep
from multiprocessing import Process, Array, Value
import RPi.GPIO as GPIO
import curses


class Motor:
    dir = 0
    pul = 0
    steerDir=1
    steerOffset=0.0


steerDir=Array('i',[1,1])
steerOffset=Array('d',[0.0,0.0])

m1=Motor()
m1.dir=26
m1.pul=19

m2=Motor()
m2.dir=20
m2.pul=16

motors=[m1,m2]

CW = 1
CCW = 0
MDIV = 2
SPRB = 200
SPR = SPRB*MDIV
SPEED=15
pSPEED=SPEED
GPIO.setmode(GPIO.BCM)
for i in range(2):
    GPIO.setup(motors[i].dir,GPIO.OUT)
    GPIO.setup(motors[i].pul,GPIO.OUT)

step_count = SPR
delay = 1.0/(SPEED*SPR)
print "delay = " + str(delay)

def turn(motor, rotations, SPEED, direction):
    #print "Direction "+str(direction)
    GPIO.output(motor.dir,direction)
    delay = 1.0/(SPEED*SPR)
    for x in range(int(rotations*SPR)):
        GPIO.output(motor.pul, GPIO.HIGH)
        sleep(delay)
        GPIO.output(motor.pul, GPIO.LOW)
        sleep(delay)
        #print "Step"


def pulse(motorId,rotations, SPEED, direction):
     
    motor=motors[motorId]
    print 'pul steerDir:'+str(steerDir[motorId]) + " direction:"+str(direction)
    if direction == steerDir[motorId]:
        print 'pul if'
        turn(motor, rotations, pSPEED, direction)
        steerOffset[motorId]+=rotations
    else:
        print 'pul else'
        print 'steerOffset:'+str(steerOffset[motorId])+" direction:"+str(direction)
        turn(motor, steerOffset[motorId], pSPEED, direction)
        steerOffset[motorId]=0.0
        steerDir[motorId]=direction


def motorMain():


    screen = curses.initscr()
    curses.noecho()  
    curses.cbreak()
    curses.halfdelay(3)
    screen.keypad(True)

    pulseRotations=0.2


    try:
        while True:   
            char = screen.getch()
            p1=p2=Process()
            p1.start()
            if char == ord('q'):
                break
            elif char in [curses.KEY_UP]:
                p1=Process(target=pulse,args=(0,pulseRotations,pSPEED, CW))
                p2=Process(target=pulse,args=(1,pulseRotations,pSPEED, CW))
                p1.start()
                p2.start()
            elif char in [curses.KEY_DOWN]:
                print 'k Down'
                p1=Process(target=pulse,args=(0,pulseRotations,pSPEED, CCW))
                p2=Process(target=pulse,args=(1,pulseRotations,pSPEED, CCW))
                p1.start()
                p2.start()
            elif char in [curses.KEY_LEFT]:
                p1=Process(target=pulse,args=(0,pulseRotations,pSPEED, CW))
                p2=Process(target=pulse,args=(1,pulseRotations,pSPEED, CCW))
                p1.start()
                p2.start()
            elif char in [curses.KEY_RIGHT]:
                p1=Process(target=pulse,args=(0,pulseRotations,pSPEED, CCW))
                p2=Process(target=pulse,args=(1,pulseRotations,pSPEED, CW))
                p1.start()
                p2.start()

            elif char in [ord('w')]:
                p1=Process(target=turn,args=(motors[0],0.1,SPEED, CW))
                p2=Process(target=turn,args=(motors[1],0.1,SPEED, CW))
                p1.start()
                p2.start()
            elif char in [ord('s')]:
                p1=Process(target=turn,args=(motors[0],0.1,SPEED, CCW))
                p2=Process(target=turn,args=(motors[1],0.1,SPEED, CCW))
                p1.start()
                p2.start()
            elif char in [ord('a')]:
                p1=Process(target=turn,args=(motors[0],0.1,SPEED, CW))
                p2=Process(target=turn,args=(motors[1],0.1,SPEED, CCW))
                p1.start()
                p2.start() 
            elif char in [ord('d')]:
                p1=Process(target=turn,args=(motors[0],0.1,SPEED, CCW))
                p2=Process(target=turn,args=(motors[1],0.1,SPEED, CW))
                p1.start()
                p2.start()
            p1.join()
            p2.join()
    finally:
        #Close down curses properly, inc turn echo back on!
        curses.nocbreak(); screen.keypad(0); curses.echo()
        curses.endwin()
        GPIO.cleanup()


def yawBy(rotations):
    if rotations > 0:
        yawLeftBy(rotations)
    else:
        yawRightBy(-rotations)

def yawLeftBy(rotations):
    p1 = Process(target=turn, args=(motors[0], rotations, SPEED, CW))
    p2 = Process(target=turn, args=(motors[1], rotations, SPEED, CCW))
    p1.start()
    p2.start()
    p1.join()
    p2.join()

def yawRightBy(rotations):
    p1 = Process(target=turn, args=(motors[0], rotations, SPEED, CCW))
    p2 = Process(target=turn, args=(motors[1], rotations, SPEED, CW))
    p1.start()
    p2.start()
    p1.join()
    p2.join()

def gyroTurnFeedback(gyroZ):
    err=gyroZ[0]
    yawBy(err/10)
