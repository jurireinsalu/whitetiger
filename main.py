import time
import gyro
import motor
import simple_pid
from multiprocessing import Process, Array, Value


# gyro.gyroMain()

gyroZ=Array('d',[0.0])


motorProcess=Process(target=motor.motorMain,args=())
motorProcess.start()

gyroInterval=0.001
gyroProcess=Process(target=gyro.readGyroZ, args=(gyroZ,gyroInterval))
gyroProcess.start()

pid=simple_pid.PID(0.0001,0,0.000)

while True:
    yawBy=pid(gyroZ[0]-30)
    motor.yawBy(gyroZ[0]/float(1000))
    print "###########\n"
    print gyroZ[0]
    print "\n############\n"
    time.sleep(0.1)

