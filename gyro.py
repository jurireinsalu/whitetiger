#!/usr/bin/python
import smbus
import math
import time
 
# Register
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
 
def read_byte(reg):
    return bus.read_byte_data(address, reg)
 
def read_word(reg):
    h = bus.read_byte_data(address, reg)
    l = bus.read_byte_data(address, reg+1)
    value = (h << 8) + l
    return value
 
def read_word_2c(reg):
    val = read_word(reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
 
def dist(a,b):
    return math.sqrt((a*a)+(b*b))
 
def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)
 
def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)
 
bus = smbus.SMBus(1) # bus = smbus.SMBus(0) fuer Revision 1
address = 0x68       # via i2cdetect
 
# Aktivieren, um das Modul ansprechen zu koennen
bus.write_byte_data(address, power_mgmt_1, 0)

def gyroMain():
    while True:


        gyroskop_xout = read_word_2c(0x43)
        gyroskop_yout = read_word_2c(0x45)
        gyroskop_zout = read_word_2c(0x47)

        acceleration_xout = read_word_2c(0x3b)
        acceleration_yout = read_word_2c(0x3d)
        acceleration_zout = read_word_2c(0x3f)

        acceleration_xout_skaliert = acceleration_xout / 16384.0
        acceleration_yout_skaliert = acceleration_yout / 16384.0
        acceleration_zout_skaliert = acceleration_zout / 16384.0
        
        sphere=math.sqrt(acceleration_xout**2 + acceleration_yout**2 + acceleration_zout**2)

        xRotationG=get_x_rotation(acceleration_xout_skaliert, acceleration_yout_skaliert, acceleration_zout_skaliert)
        yRotationG=get_y_rotation(acceleration_xout_skaliert, acceleration_yout_skaliert, acceleration_zout_skaliert)
    
        print "\n\nGyro"
        print "--------"
 
        print "gyro_xout: ", ("%5d" % gyroskop_xout), " skaliert: ", (gyroskop_xout / 131)
        print "gyro_yout: ", ("%5d" % gyroskop_yout), " skaliert: ", (gyroskop_yout / 131)
        print "gyro_zout: ", ("%5d" % gyroskop_zout), " skaliert: ", (gyroskop_zout / 131)
 
        print
        print "Acceleration sensor"
        print "---------------------"
 
     
     
        print "Acceleration_xout: ", ("%6d" % acceleration_xout), " skaliert: ", acceleration_xout_skaliert
        print "Acceleration_yout: ", ("%6d" % acceleration_yout), " skaliert: ", acceleration_yout_skaliert
        print "Acceleration_zout: ", ("%6d" % acceleration_zout), " skaliert: ", acceleration_zout_skaliert
        print "Acceleration sphere:", sphere

        print "X Rotation: " , xRotationG
        print "Y Rotation: " , yRotationG

def readGyroZ(gyroZ,interval):
    while True:
        gyroskop_zout = read_word_2c(0x47)
        gyroZ[0]=gyroskop_zout
        time.sleep(interval)
